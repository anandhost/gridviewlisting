from django.contrib import admin
from firstgoal.models import *

admin.site.register(header)
admin.site.register(Category)
admin.site.register(Page)
admin.site.register(Profile)
admin.site.register(socialmedia)
