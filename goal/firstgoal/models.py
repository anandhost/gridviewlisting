from __future__ import unicode_literals

from django.db import models

class header(models.Model):
	name = models.CharField(max_length=128, unique=True)
	logo = models.ImageField(upload_to = 'static/logo/', blank=True, null=True)

	def __unicode__(self):
		return self.name

class Category(models.Model):
	name = models.CharField(max_length=128, unique=True)

	def __unicode__(self):
		return self.name

class Page(models.Model):
	DEFAULT_ID = 1
	category = models.ForeignKey(Category,default=DEFAULT_ID)
	title = models.CharField(max_length=128)
	url = models.URLField()
	views = models.IntegerField(default=0)

	def __unicode__(self):
		return self.title

class Profile(models.Model):
    first_name = models.CharField(max_length=30)
    last_name = models.CharField(max_length=30,blank=True)
    photo = models.ImageField(upload_to = 'static/images/',blank=True, null=True)
    job = models.CharField(max_length=50, blank=True, null=True)
    location = models.CharField(max_length=100, blank=True, null=True)
    about = models.TextField(blank=True, null=True)
    
    Email = models.EmailField(default="anand@nextpixar.com")
    Mobile = models.IntegerField(default=0000000000)
    Website = models.URLField(default="www.anandprakash.xyz")


    def __unicode__(self):
    	return self.first_name

class socialmedia(models.Model):
	facebook = models.CharField(max_length=50)
	twitter = models.CharField(max_length = 50)
	google = models.CharField(max_length = 100)
	linkedin = models.CharField(max_length = 100)

	def __unicode__(self):
		return self.facebook