from django.conf.urls import url,include, patterns

import views

urlpatterns = patterns('',url(r'^$',views.index, name='index'), url(r'^about/$', views.about, name='about'))
